#pragma once

#include "stdafx.h"
#include "EASTL/hash_set.h"
#include "mpirxx.h"

void* __cdecl operator new[](size_t size, const char*, int, unsigned, const char*, int);
void* __cdecl operator new[](size_t size, unsigned int, unsigned int, char const *, int, unsigned int, char const *, int);

struct hash_mpz
{
	size_t operator()(const mpz_class & val) const;
};


class GameManager {
public:
	mpz_class * coordinates;
	mpz_class * neighbours;
	eastl::hash_set< mpz_class, hash_mpz > livingCells;

	mpz_class combineCoordinates(mpz_class x, mpz_class y);
	mpz_class * getCoordinates(mpz_class z);

	bool isAlive(mpz_class x, mpz_class y);
	void setAlive(mpz_class x, mpz_class y);
	void setDead(mpz_class x, mpz_class y);
	void toggleAlive(mpz_class x, mpz_class y);

	void turn();
	void turn_ForeachChangedCell(void(*callback)(mpz_class *));

	mpz_class * enumerateNeighbours(mpz_class x, mpz_class y);
	mpz_class * enumerateNeighbours(mpz_class cell);
};