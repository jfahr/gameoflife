# GameOfLife

Just another implementation of Conway's Game of Life. Supports an infinite playing field (as long as you have enough memory).

## Dependencies

Depends on the [EA Standard Template Library](https://github.com/electronicarts/EASTL) (EASTL) and the [GNU Multiple Precision Arithmetic Library](https://gmplib.org/) (GMP).
Install instructions will be added in a later commit.