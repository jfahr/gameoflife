// GameOfLifeManager.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "mpirxx.h"
#include "EASTL/hash_set.h"
#include "EASTL/iterator.h"

// Operator overloads required by EASTL
void* __cdecl operator new[](size_t size, const char*, int, unsigned, const char*, int)
{
	return new uint8_t[size];
}
// Operator overloads required by EASTL
void* __cdecl operator new[](size_t size, unsigned int, unsigned int, char const *, int, unsigned int, char const *, int)
{
	return new uint8_t[size];
}

struct hash_mpz 
{
	size_t operator()(const mpz_class& val) const
	{
		return val.get_ui();
	}
};


class GameManager {
	// Whenever getCoordinates is called, this array will hold the returned values.
	mpz_class coordinates[2] = {};

	// Whenever getCoordinates is called, this array will hold the returned values.
	mpz_class neighbours[8] = {};

	eastl::hash_set< mpz_class, hash_mpz > livingCells;

	// Combine two coordinates into a single, unique number.
	mpz_class combineCoordinates(mpz_class x, mpz_class y)
	{
		if (x < y) {
			return y * y + x;
		}
		else {
			return x * x + x + y;
		}
	}

	// Find the two coordinates that are represented by this
	// single number. Results are in global  mpz_class* coordinates
	void getCoordinates(mpz_class z)
	{
		mpz_class sqrt_z = sqrt(z);
		mpz_class sqrt_z_sqr = sqrt_z * sqrt_z;

		if (z - sqrt_z_sqr < sqrt_z) {
			coordinates[0] = z - sqrt_z_sqr;
			coordinates[1] = sqrt_z;
		}
		else {
			coordinates[0] = sqrt_z;
			coordinates[1] = z - sqrt_z_sqr - sqrt_z;
		}
	}

	// Indicates whether the cell at the specified coordinates is alive.
	bool isAlive(mpz_class x, mpz_class y)
	{
		mpz_class pair = combineCoordinates(x, y);
		return livingCells.count(pair);
	}
	// Brings the cell at the specified coordinates to life.
	// Does nothing if the cell is already alive.
	void setAlive(mpz_class x, mpz_class y)
	{
		mpz_class pair = combineCoordinates(x, y);
		livingCells.insert(pair);
	}
	// Kills the cell at the specified coordinates.
	// Does nothing if the cell is already dead.
	void setDead(mpz_class x, mpz_class y)
	{
		mpz_class pair = combineCoordinates(x, y);
		livingCells.erase(pair);
	}
	// Kills the cell at the specifed coordinates if it is currently alive.
	// Otherwise, brings that cell to life.
	void toggleAlive(mpz_class x, mpz_class y)
	{
		mpz_class pair = combineCoordinates(x, y);
		if (livingCells.count(pair)) {
			livingCells.erase(pair);
		}
		else {
			livingCells.insert(pair);
		}
	}

	// Execute a single game turn.
	void turn()
	{
		eastl::hash_set<mpz_class, hash_mpz> queue;  // Cells that will be processed in the future
		queue = livingCells;
		eastl::hash_set<mpz_class, hash_mpz> done;  // Cells that have already been processed, to avoid redundancy
		eastl::hash_set<mpz_class, hash_mpz> nextTurnLivingCells;  // Cells that will be alive after this turn
		mpz_class cell;

		while (!queue.empty()) {
			// Get any cell from the queue
			cell = *queue.begin();
			if (done.count(cell)) {
				queue.erase(cell);
				continue;
			}

			int countLivingNeighbours = 0;
			enumerateNeighbours(cell);

			if (livingCells.count(cell)) {
				// Look at all neighbours of this cell
				for (int i = 0; i < 9; ++i) {
					countLivingNeighbours += livingCells.count(neighbours[i]);
					queue.insert(neighbours[i]);  // Any neighbour of a living cell should also be processed
				}
				// Check if this cell will be alive next round
				if (countLivingNeighbours == 2 || countLivingNeighbours == 3) {
					nextTurnLivingCells.insert(cell);
				}
			}
			else {
				// Look at all neighbours of this cell
				for (int i = 0; i < 9; ++i) {
					countLivingNeighbours += livingCells.count(neighbours[i]);
					if (countLivingNeighbours > 3) {
						break;
					}
				}
				// Check if this cell will be alive next round
				if (countLivingNeighbours == 3) {
					nextTurnLivingCells.insert(cell);
				}
			}
			queue.erase(cell);
			done.insert(cell);  // Add this cell to done so it won't be processed again
		}
		livingCells = nextTurnLivingCells;
	}
	
	// Execute a single game turn and call a function for every cell 
	// that changes state in this turn.
	void turn_ForeachChangedCell(void(*callback)(mpz_class *))
	{
		eastl::hash_set<mpz_class, hash_mpz> queue;  // Cells that will be processed in the future
		queue = livingCells;
		eastl::hash_set<mpz_class, hash_mpz> done;  // Cells that have already been processed, to avoid redundancy
		eastl::hash_set<mpz_class, hash_mpz> nextTurnLivingCells;  // Cells that will be alive after this turn
		mpz_class cell;

		while (!queue.empty()) {
			// Get any cell from the queue
			cell = *queue.begin();
			if (done.count(cell)) {
				queue.erase(cell);
				continue;
			}

			int countLivingNeighbours = 0;
			enumerateNeighbours(cell);

			if (livingCells.count(cell)) {  // If cell is alive
				// Look at all neighbours of this cell
				for (int i = 0; i < 9; ++i) {
					countLivingNeighbours += livingCells.count(neighbours[i]);
					queue.insert(neighbours[i]);  // Any neighbour of a living cell should also be processed
				}
				// Check if this cell will be alive next round
				if (countLivingNeighbours == 2 || countLivingNeighbours == 3) {
					nextTurnLivingCells.insert(cell);
				}
				else {
					// enumerateNeighbours(cell) already implicitly called getCoordinates(cell)
					callback(coordinates);
				}
			}
			else {  // If cell is dead
				// Look at all neighbours of this cell
				for (int i = 0; i < 9; ++i) {
					countLivingNeighbours += livingCells.count(neighbours[i]);
					if (countLivingNeighbours > 3) {
						break;
					}
				}
				// Check if this cell will be alive next round
				if (countLivingNeighbours == 3) {
					callback(coordinates);
					nextTurnLivingCells.insert(cell);
				}
			}
			queue.erase(cell);
			done.insert(cell);  // Add this cell to done so it won't be processed again
		}
		livingCells = nextTurnLivingCells;
	}

	// Enumerate the neighbours of the specified cell.
	// Results are in global  mpz_class* neighbours
	void enumerateNeighbours(mpz_class x, mpz_class y)
	{
		neighbours[0] = combineCoordinates(x - 1, --y);
		neighbours[1] = combineCoordinates(x,       y);
		neighbours[2] = combineCoordinates(x + 1,   y);
		neighbours[3] = combineCoordinates(x - 1, ++y);
		neighbours[4] = combineCoordinates(x + 1,   y);
		neighbours[5] = combineCoordinates(x - 1, ++y);
		neighbours[6] = combineCoordinates(x,       y);
		neighbours[7] = combineCoordinates(x + 1,   y);
	}
	void enumerateNeighbours(mpz_class cell)
	{
		getCoordinates(cell);
		enumerateNeighbours(coordinates[0], coordinates[1]);
	}
};